import UIKit

public class HWUIKit {
        
    public static let shared = HWUIKit(with: Configuration.default)
    
    private(set) public var configuration: Configuration!
    
    public lazy var colors: Colors = { getColors() }()
    public lazy var images: Images = { getImages() }()

    init(with configuration: Configuration) {
        self.configuration = configuration
    }
    
    public let resourceBundle: Bundle = {
        return Bundle.module
    }()
    
}

extension HWUIKit {
    private func getColors() -> Colors {
        return DefaultColors()
    }

    private func getImages() -> Images {
        return Images()
    }
}

public class Configuration {

   public static let `default` = Configuration()

}
