//
//  UIImage+Extensions.swift
//  HWUIKit
//
//  Created by Kimi on 21/10/21.
//

import Foundation
import UIKit

public extension UIImage {
    
    func jpegDataCompressedTo(bytes: Int) -> Data? {
        var compressionQuality: CGFloat = 1.0
        guard var jpegData = self.jpegData(compressionQuality: compressionQuality) else {
            return nil
        }
        while jpegData.count > bytes, compressionQuality > 0.0 {
            compressionQuality -= 0.1
            guard let compressedData = self.jpegData(compressionQuality: compressionQuality) else {
                return nil
            }
            jpegData = compressedData
        }
        return jpegData
    }
    
    func jpegDataCompressedTo(megabytes: Double) -> Data? {
        let sizeInBytes = megabytes * 1024.0 * 1024.0
        var needCompress:Bool = true
        var imgData:Data?
        var compressingValue:CGFloat = 1.0
        while (needCompress && compressingValue > 0.0) {
            if let data:Data = jpegData(compressionQuality: compressingValue) {
                if data.count < Int(sizeInBytes) {
                    needCompress = false
                    imgData = data
                } else {
                    compressingValue -= 0.1
                }
            }
        }

        if let data = imgData {
            if (data.count < Int(sizeInBytes)) {
                return data
            }
        }
        return nil
    }
    
    static func fixOrientation(for image: UIImage) -> UIImage {
        
        guard image.imageOrientation != UIImage.Orientation.up
            , let cgImage = image.cgImage
            , let colorSpace = cgImage.colorSpace else {
                return image
        }
        var transform = CGAffineTransform.identity
        
        switch image.imageOrientation {
        case .down, .downMirrored:
            transform = transform.translatedBy(x: image.size.width, y: image.size.height)
            transform = transform.rotated(by: CGFloat(Double.pi))
            
        case .left, .leftMirrored:
            transform = transform.translatedBy(x: image.size.width, y: 0)
            transform = transform.rotated(by: CGFloat(Double.pi / 2))
            
        case .right, .rightMirrored:
            transform = transform.translatedBy(x: 0, y: image.size.height)
            transform = transform.rotated(by: CGFloat(-Double.pi / 2))
            
        default: break
        }
        
        switch image.imageOrientation {
        case .upMirrored, .downMirrored:
            transform = transform.translatedBy(x: image.size.width, y: 0)
            transform = transform.scaledBy(x: -1, y: 1)
            
        case .leftMirrored, .rightMirrored:
            transform = transform.translatedBy(x: image.size.height, y: 0)
            transform = transform.scaledBy(x: -1, y: 1)
            
        default: break
        }
        
        guard let ctx = CGContext(data: nil
                                , width: Int(image.size.width)
                                , height: Int(image.size.height)
                                , bitsPerComponent: cgImage.bitsPerComponent
                                , bytesPerRow: 0
                                , space: colorSpace
                                , bitmapInfo: cgImage.bitmapInfo.rawValue) else {
            return image
        }
        ctx.concatenate(transform)
        
        switch (image.imageOrientation) {
        case .left, .leftMirrored, .right, .rightMirrored:
            ctx.draw(cgImage, in: CGRect(x: 0,y: 0,width: image.size.height,height: image.size.width))
            
        default:
            ctx.draw(cgImage, in: CGRect(x: 0,y: 0,width: image.size.width,height: image.size.height))
        }
        
        guard let orientedImage = ctx.makeImage() else {
            return image
        }
        return UIImage(cgImage: orientedImage)
    }
}
