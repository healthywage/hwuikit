//
// Copyright © 2021 HealthyWage. All rights reserved.
//

import Foundation
import UIKit

/// Custom and versatile implementation to group a set of views
/// with a set of configurations to be able to use vertical and horizontal scrolls
/// with all posible orientations

public class ScrollableListView: NonStoryboardableView {

    /// Desired scroll direction,
    /// indicating how users finger should scroll
    public enum ScrollDirection {
        /// Reverse vertical scroll where `contentOffset.y` is `contentSize.maxY`
        case upToDown
        /// Natural vertical scroll where `contentOffset.y`  is 0
        case downToUp
        /// Reverse horizontal scroll where `contentOffset.x` is `+contentSize.maxX`
        case rightToLeft
        /// Natural horizontal scroll where `contentOffset.x`  is 0
        case leftToRight
    }

    private let viewDistribution: NSLayoutConstraint.Axis
    private let scrollDirection: ScrollDirection
    private let contentInset: CGFloat
    public init(
        viewDistribution: NSLayoutConstraint.Axis,
        scrollDirection: ScrollDirection = .downToUp,
        contentInset: CGFloat
    ) {
        self.viewDistribution = viewDistribution
        self.scrollDirection = scrollDirection
        self.contentInset = contentInset
        super.init()

        addSubview(scrollView)
        scrollView.addSubview(stackView)
    }

    // To take in consideration that this only works,
    // if each item use all the size of the scroll direction
    // eg: if its horizontal scrollable: the item should use the entire visible width of the scrollView
    public var isPagingEnabled: Bool = false {
        didSet { scrollView.isPagingEnabled = isPagingEnabled }
    }

    public lazy var scrollView: UIScrollView = {
        var scrollView: UIView = UIScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        transformViewIfNeeded(&scrollView)

        let inset: UIEdgeInsets
        switch scrollDirection {
        case .rightToLeft, .leftToRight:
            inset = .init(top: 0, left: contentInset, bottom: 0, right: contentInset)
        case .upToDown, .downToUp:
            inset = .init(top: contentInset, left: 0, bottom: contentInset, right: 0)
        }

        guard let scrollView = scrollView as? UIScrollView else {
            assertionFailure("Should succeed")
            return UIScrollView()
        }

        scrollView.contentInset = inset

        return scrollView
    }()

    private lazy var stackView = StackView(orientation: viewDistribution)

    public func addArrangedSubview(_ view: UIView, withCustomSpacing spacing: CGFloat) {
        var viewToAdd = view
        transformViewIfNeeded(&viewToAdd)
        stackView.addArrangedSubview(viewToAdd)
        stackView.setCustomSpacing(spacing, after: viewToAdd)
    }

    public func removeAllArrangedSubviews() {
        stackView.removeAllArrangedSubviews()
    }

    public func scrollToLastItem() {
        scrollView.setContentOffset(.zero, animated: false)
    }

    public func scrollTo(_ point: CGPoint) {
        let pointToSet: CGPoint
        switch scrollDirection {
        case .upToDown:
            pointToSet = CGPoint(x: 0, y: -point.y)
        case .downToUp:
            pointToSet = CGPoint(x: 0, y: point.y)
        case .rightToLeft:
            pointToSet = CGPoint(x: point.x, y: 0)
        case .leftToRight:
            pointToSet = CGPoint(x: -point.x, y: 0)
        }
        scrollView.setContentOffset(pointToSet, animated: false)
    }

    // Helper method to make a vertical scrollable list with fixed number of items per row

    // horizontal stackView that will contains 2 subscription items each
    var horizontalStackViews: [StackView] = []

//    private var onItemSelection: ((ItemViewModel) -> Void)

    public func addViews(
        _ views: [UIView],
        perRow amount: Int,
        spacingBetweenRows spacing: CGFloat = 8,
        removingOldOnes: Bool = true
    ) {
        if removingOldOnes {
            stackView.removeAllArrangedSubviews()
            horizontalStackViews.forEach { $0.removeAllArrangedSubviews() }
        }

        var viewsToAddHorizontally: [UIView] = []
        views.forEach {
            viewsToAddHorizontally.append($0)
            if viewsToAddHorizontally.count == amount {
                let horizontal = StackView(
                    orientation: .horizontal,
                    distribution: .fillEqually
                )
                horizontal.addArrangedSubviews(from: viewsToAddHorizontally)
                horizontal.spacing = spacing
                viewsToAddHorizontally.removeAll()
                horizontalStackViews.append(horizontal)
            }
        }

        stackView.addArrangedSubviews(from: horizontalStackViews)
        stackView.spacing = spacing
    }


    // MARK: - UpdateConstraints
    public override func updateConstraints() {
        super.updateConstraints()

        scrollView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }

        stackView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
            switch viewDistribution {
            case .horizontal:
                make.height.equalToSuperview()
            case .vertical:
                make.width.equalToSuperview()
            @unknown default:
                assertionFailure()
                return
            }
        }
    }

    // MARK: - Private

    private func transformViewIfNeeded(_ view: inout UIView) {
        // For these cases we do a simply trick,
        // transforming to rotate to oposite direction form the natural one,
        // *we must rotate the inner views later on, on the same direction*,
        // otherwise they will look reversed as well.
        switch scrollDirection {
        case .leftToRight:
            view.transform = .init(scaleX: -1, y: 1)
        case .upToDown:
            view.transform = .init(scaleX: 1, y: -1)
        default:
            break
        }
    }
}
