//
//  File.swift
//  
//
//  Created by David Muñoz on 28/10/2022.
//

import UIKit

public class IconButton: NonStoryboardableControl {
    public var image: UIImage {
        didSet { imageView.image = image }
    }

    public init(image: UIImage) {
        self.image = image
        imageView = .init(image: image, highlightedImage: nil)
        super.init()
        addSubview(imageView)
        imageView.contentMode = .scaleAspectFit
    }

    private let imageView: UIImageView
    
    public override func updateConstraints() {
        super.updateConstraints()
        imageView.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.edges.greaterThanOrEqualToSuperview().inset(Sizes.smallPadding)
        }
    }
}
