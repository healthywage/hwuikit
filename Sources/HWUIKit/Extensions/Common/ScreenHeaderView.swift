//
// Copyright © 2021 HealthyWage. All rights reserved.
//


import Foundation
import UIKit

public final class ScreenHeaderView: NonStoryboardableView {

    public override init() {
        super.init()
        addSubview(titleLabel)
        backgroundColor = HWUIKit.shared.colors.blueButtonColor
    }

    public var title: String? {
        set { titleLabel.text = newValue }
        get { titleLabel.text }
    }
    
    public var font: UIFont {
        set { titleLabel.font = newValue }
        get { titleLabel.font }
    }
    
    private let titleLabel = HWLabel(withStyle: .bold(withSize: 21), andTextColor: .white)
    
    
    public override func updateConstraints() {
        super.updateConstraints()
        titleLabel.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.top.bottom.equalToSuperview().inset(6)
        }
    }
}
