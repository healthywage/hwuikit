//
//  Label.swift
//  HWUIKit
//
//  Created by David Muñoz on 08/12/2021.
//

import UIKit

/// A base for labels that do not support NIBs/storyboards, so there is no need in defining
/// `required init?(coder:)` initializer. It also declares Auto Layout support,
/// with defined font styles.
public class HWLabel: UILabel {
    public enum Style {
        case regular(withSize: CGFloat)
        case bold(withSize: CGFloat)
        case condensedBold(withSize: CGFloat)
        case condensedRegular(withSize: CGFloat)
    }
    
    public var style: Style {
        didSet { updateStyles() }
    }
    
    public var isUnderlined: Bool {
        didSet { updateStyles() }
    }
    
    public override var text: String? {
        didSet {
            self.updateStyles()
        }
    }
    public init(withStyle style: Style, andTextColor textColor: UIColor, isUnderlined: Bool = false) {
        self.style = style
        self.isUnderlined = isUnderlined
        super.init(frame: .zero)
        self.textColor = textColor
        
        updateStyles()
        translatesAutoresizingMaskIntoConstraints = false
    }
    
    private func updateStyles() {
        switch style {
        case .regular(withSize: let size):
            font = HWFont.regularSoftFontOfSize(size)
        case .bold(withSize: let size):
            font = HWFont.boldSoftFontOfSize(size)
        case .condensedBold(withSize: let size):
            font = HWFont.boldCondensedOfSize(size)
        case .condensedRegular(withSize: let size):
            font = HWFont.regularCondensedOfSize(size)
        }
        
        if isUnderlined {
            let attributedText = NSAttributedString(
                string: text ?? "",
                attributes: [.underlineStyle: NSUnderlineStyle.thick.rawValue]
            )
            self.attributedText = attributedText
        }
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
