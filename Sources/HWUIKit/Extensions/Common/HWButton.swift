//
//  HWButton.swift
//  HWUIKit
//
//  Created by David Muñoz on 09/12/2021.
//

import Foundation
import UIKit

/// Common styled button with rounded corners button
/// that can contains a small subtitle

public class NewHWButton: NonStoryboardableControl {
    public enum Style {
        /// Orange colored background with white font
        case orange
        /// Blue colored background with white font
        case blue
        /// Gray colored background with dark gray font
        case gray
        /// Clear background with red font
        case linkRed
        case linkOrange
        case link
        
    }
    
    public enum Size {
        case big
        case medium
        case small
    }
    
    private let titleLabel: HWLabel
    private let subtitleLabel: HWLabel
    private let style: Style
    private let size: Size

    public var textColor: UIColor {
        didSet {
            titleLabel.textColor = textColor
            subtitleLabel.textColor = textColor
        }
    }

    public init(
        withTitle title: String,
        size: Size = .medium,
        subtitle: String? = nil,
        style: Style = .orange
    ) {
        self.style = style
        self.size = size
        
        let fontColor: UIColor
        let fontStyle: HWLabel.Style
        switch style {
        case .gray:
            fontColor = UIColor.darkGray
            fontStyle = .bold(withSize: 17)
        case .linkRed:
            fontColor = UIColor.red
            fontStyle = .regular(withSize: 17)
        case .linkOrange:
            fontColor = HWUIKit.shared.colors.mainOrange
            fontStyle = .bold(withSize: 17)
        case .link:
            if #available(iOS 13.0, *) {
                fontColor = UIColor.link
            } else {
                fontColor = UIColor.blue
            }
            fontStyle = .regular(withSize: 17)
        default:
            fontColor = .white
            fontStyle = .bold(withSize: 17)
        }

        titleLabel = HWLabel(withStyle: fontStyle, andTextColor: fontColor)
        titleLabel.isUnderlined = style == .linkRed || style == .linkOrange || style == .link
        titleLabel.textAlignment = .center
        titleLabel.text = title

        subtitleLabel = HWLabel(withStyle: .bold(withSize: 12), andTextColor: fontColor)
        subtitleLabel.isHidden = subtitle == nil
        subtitleLabel.textAlignment = .center
        subtitleLabel.text = subtitle

        self.textColor = fontColor
        super.init()
        
        addSubview(titleLabel)
        addSubview(subtitleLabel)
        addLayoutGuide(labelsLayoutGuide)
        applyStyle()
    }
    
    // Guide to be placed at the center of the button
    // Helping to the labels to take place
    private let labelsLayoutGuide = UILayoutGuide()
    
    private func applyStyle() {
        layer.cornerRadius = Sizes.regularCornerRadius
        let colors = HWUIKit.shared.colors

        switch style {
        case .orange:
            backgroundColor = colors.orangeButtonColor
        case .blue:
            backgroundColor = colors.blueButtonColor
        case .gray:
            backgroundColor = colors.lightGrayButtonColor
        case .linkRed, .link, .linkOrange:
            backgroundColor = .clear
        }
    }
    
    // MARK: - UpdateConstraints

    public override func updateConstraints() {
        super.updateConstraints()
        labelsLayoutGuide.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
        }
        
        titleLabel.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.left.right.equalToSuperview()
                .inset(Sizes.largePadding)
                .priority(.medium)
            make.top.equalToSuperview()
                .offset(verticalPadding)
                .priority(.medium)

            if subtitleLabel.isHidden {
                make.bottom.equalToSuperview().inset(verticalPadding)
                make.centerY.equalTo(labelsLayoutGuide.snp.centerY).priority(.low)
            } else {
                make.centerY.equalTo(labelsLayoutGuide.snp.centerY)
                    .offset(-Sizes.smallPadding)
            }
        }
        
        guard !subtitleLabel.isHidden else {
            return
        }
        
        subtitleLabel.snp.makeConstraints { make in
            make.left.right.equalTo(titleLabel)
            make.top.equalTo(titleLabel.snp.bottom)
            make.bottom.lessThanOrEqualToSuperview()
                .inset(verticalPadding)
                .priority(.medium)
        }
    }
    
    private var verticalPadding: CGFloat {
        switch size {
        case .small:
            return Sizes.smallPadding
        case .medium:
            return Sizes.normalPadding
        case .big:
            return Sizes.largePadding
        }
    }
    
    // MARK: - Overrides

    public override var isHighlighted: Bool {
        didSet { updateUI() }
    }
    
    public override var isEnabled: Bool {
        didSet { updateUI() }
    }

    // MARK: - Private

    private func updateUI() {
        if isEnabled {
            isHighlighted ? applyHighlightedState() : applyNormalState()
        } else {
            applyDisabledState()
        }
    }
    
    private func applyHighlightedState() {
        self.transform = .init(scaleX: 0.9, y: 0.9)
    }
    
    private func applyDisabledState() {
        let colors = HWUIKit.shared.colors
        self.backgroundColor = colors.disabledButton
    }
    
    private func applyNormalState() {
        self.transform = .identity
        applyStyle()
    }
}
