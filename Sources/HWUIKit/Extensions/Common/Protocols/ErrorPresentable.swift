//
//  ErrorPresentable.swift
//  HWUIKit
//
//  Created by David Muñoz on 14/12/2021.
//
import UIKit

/// Simple protocol to group common error message presentation
public protocol ErrorPresentable {
    func presentErrorMessage(_ error: String, onCompletion: (()->Void)?)
}

/// Default implementation of method can be override
public extension ErrorPresentable where Self: UIViewController {
    func presentErrorMessage(_ errorMessage: String, onCompletion: (()->Void)? = nil ) {
        let alert = UIAlertController(title: errorMessage, message: nil, preferredStyle: .alert)
        alert.addAction(
            .init(title: "Ok", style: .cancel, handler: { _ in
                onCompletion?()
            })
        )
        present(alert, animated: true)
    }
}
