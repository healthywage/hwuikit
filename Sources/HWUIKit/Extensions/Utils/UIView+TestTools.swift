//
//  File.swift
//  
//
//  Created by David Muñoz on 30/06/2023.
//

import UIKit

public extension UIView {
    enum PingingBorders {
        case top
        case bottom
        case left
        case right
    }
    
    func activeDebugBorder() {
        let colors: [UIColor] = [.blue, .yellow, .orange, .purple]
        self.layer.borderWidth = 1
        self.layer.borderColor = colors.randomElement()?.cgColor
    }
    
    func containedInAView(
        piningEdges: [PingingBorders] = [.left, .top, .left, .right],
        backgroundColor: UIColor = .clear,
        ofSize size: CGSize = .init(width: 750, height: 1334)
    ) -> UIView {
        let view = UIView(frame: .init(origin: .zero, size: size))
        view.backgroundColor = backgroundColor
        view.addSubview(self)
        
        self.snp.makeConstraints { maker in
            piningEdges.forEach {
                switch $0 {
                case .top:
                    maker.top.equalToSuperview()
                case .bottom:
                    maker.bottom.equalToSuperview()
                case .left:
                    maker.left.equalToSuperview()
                case .right:
                    maker.right.equalToSuperview()
                }
            }
        }
        return view
    }
}
