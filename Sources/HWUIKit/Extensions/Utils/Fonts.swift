//
//  Fonts.swift
//  HWUIKit
//
//  Created by David Muñoz on 08/12/2021.
//

// TODO:  Fix to use local fonts, not main bundle ones
import UIKit

struct HWFont {
    private static let regularFontName = "ProximaNova-Regular"
    private static let boldFontName = "ProximaNovaA-Bold"
    private static let regularSoftFontName = "ProximaNovaSoft-Regular"
    private static let softCondensedName = "ProximaSoftCond-Medium"
    private static let softCondensedBoldName = "ProximaSoftCond-Bold"
    private static let boldSoftFontName = "ProximaNovaSoft-Bold"
    private static let regularItalicFontName = "Proxima-Nova-Regular-Italic"
    private static let lightItalicFontName = "Proxima-Nova-Light-Italic"
    private static let fontOwasom = "FontAwesome"
    private static let fontHandsOfSean = "Hand Of Sean (Demo)"
    
    static func iconFontOfSize(_ size: CGFloat) -> UIFont {
        if let font = UIFont(name: fontOwasom, size: size) {
            return font
        }
        return .systemFont(ofSize: size)
    }
    
    static func regularCondensedOfSize(_ size: CGFloat) -> UIFont {
        if let font = UIFont(name: softCondensedName, size: size) {
            return font
        }
        return .systemFont(ofSize: size)
    }

    static func regularFontOfSize(_ size: CGFloat) -> UIFont {
        if let font = UIFont(name: regularFontName, size: size) {
            return font
        }
        return .systemFont(ofSize: size)
    }
    
    static func boldCondensedOfSize(_ size: CGFloat) -> UIFont {
        if let font = UIFont(name: softCondensedBoldName, size: size) {
            return font
        }
        return .systemFont(ofSize: size)
    }
    
    static func boldFontOfSize(_ size: CGFloat) -> UIFont {
        if let font = UIFont(name: boldFontName, size: size) {
            return font
        }
        return .boldSystemFont(ofSize: size)
    }
    
    static func regularSoftFontOfSize(_ size: CGFloat) -> UIFont {
        if let font = UIFont(name: regularSoftFontName, size: size) {
            return font
        }
        return .systemFont(ofSize: size)
    }
    
    static func boldSoftFontOfSize(_ size: CGFloat) -> UIFont {
        if let font = UIFont(name: boldSoftFontName, size: size) {
            return font
        }
        return .boldSystemFont(ofSize: size)
    }
    
    static func lightItalicFontOfSize(_ size: CGFloat) -> UIFont {
        if let font = UIFont(name: lightItalicFontName, size: size) {
            return font
        }
        return .italicSystemFont(ofSize: size)
    }
    
    static func regularItalicFontOfSize(_ size: CGFloat) -> UIFont {
        if let font = UIFont(name: regularItalicFontName, size: size) {
            return font
        }
        return .italicSystemFont(ofSize: size)
    }
    
    static func regularHandOfSeanFontOfSize(_ size: CGFloat) -> UIFont {
        if let font = UIFont(name: fontHandsOfSean, size: size) {
            return font
        }
        return .systemFont(ofSize: size)
    }
}
