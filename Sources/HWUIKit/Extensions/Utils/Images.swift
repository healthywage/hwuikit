//
//  File.swift
//  
//
//  Created by David Muñoz on 30/06/2023.
//

import UIKit

public struct Images {

    public var checkmark: UIImage = UIImage(systemName: "checkmark")!

    private func image(named nameString: String) -> UIImage {
        guard let image = UIImage(
            named: nameString,
            in: Bundle.module,
            compatibleWith: nil
        ) else {
            assertionFailure("Missing color")
            return .init()
        }
        return image
    }
}
