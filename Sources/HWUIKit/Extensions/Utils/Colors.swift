//
//  Colors.swift
//  Braintree
//
//  Created by David Muñoz on 06/12/2021.
//

import Foundation
import UIKit

public protocol Colors {
    var linkColor: UIColor { get }
    var grayColor: UIColor { get }

    // MARK: Buttons

    var orangeButtonColor: UIColor { get }
    var blueButtonColor: UIColor { get }
    var lightGrayButtonColor: UIColor { get }
    var disabledButton: UIColor { get }
    var mainOrange: UIColor { get }
    var segmentedOrange: UIColor { get }
}

/// Default colors
@available(iOS 11.0, *)
public struct DefaultColors: Colors {

    public var linkColor: UIColor { color(named: "LinkColor") }
    public var grayColor: UIColor { color(named: "Gray") }
    public var orangeButtonColor: UIColor { color(named: "MainOrange") }
    public var blueButtonColor: UIColor { color(named: "BlueButtonColor") }
    public var lightGrayButtonColor: UIColor { color(named: "LightGrayButtonColor") }
    public var disabledButton: UIColor { color(named: "DisabledButton") }
    public var mainOrange: UIColor { color(named: "MainOrange") }
    public var segmentedOrange: UIColor { color(named: "SegmentedOrange") }

    private func color(named nameString: String) -> UIColor {
        guard let color = UIColor(named: nameString, in: Bundle.module, compatibleWith: nil) else {
            assertionFailure("Missing color")
            return .clear
        }
        return color
    }
}

/// Class to return in case of version below 11.0
public struct WrongVersionColors: Colors {
    public var lightGrayButtonColor: UIColor { .lightGray }
    public var linkColor: UIColor { .blue }
    public var grayColor: UIColor { .gray }
    public var orangeButtonColor: UIColor { .orange }
    public var mainOrange: UIColor { .orange }
    public var blueButtonColor: UIColor { .blue }
    public var disabledButton: UIColor { .gray }
    public var segmentedOrange: UIColor { .orange }
}
