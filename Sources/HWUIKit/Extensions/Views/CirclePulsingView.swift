//
// Copyright © 2021 HealthyWage. All rights reserved.
//

import Foundation
import UIKit

/// Orange circular border with a frame of `80x80`,
/// starts a pulsing animation when `layoutSubviews` is called
public class CirclePulsingView: NonStoryboardableView {

    private let size = 80
    public override var intrinsicContentSize: CGSize { .init(width: size, height: size)}

    public init(withColor color: UIColor) {
        super.init()
        layer.borderWidth = 3
        layer.borderColor = color.cgColor
        layer.cornerRadius = CGFloat(size/2)
        isUserInteractionEnabled = false
    }

    public override func layoutSubviews() {
        super.layoutSubviews()
        startPulsing()
    }

    private func startPulsing() {

        // Creating scale animation for the layer, from and to value should be in range of 0.0 to 1.0
        let scaleAnimation = CABasicAnimation(keyPath: "transform.scale")
        scaleAnimation.fromValue = 1.0
        scaleAnimation.toValue = 0.5


        // Grouping both animations and giving animation duration, animation repat count
        let groupAnimation = CAAnimationGroup()
        groupAnimation.animations = [scaleAnimation]
        groupAnimation.duration = 1
        groupAnimation.repeatCount = .infinity
        groupAnimation.timingFunction = CAMediaTimingFunction(name: .easeInEaseOut)
        groupAnimation.autoreverses = true

        layer.add(groupAnimation, forKey: "pulseanimation")
    }

}
