//
//  SimpleLineView.swift
//  HWUIKit
//
//  Created by David Muñoz on 07/12/2021.
//

import UIKit
import SnapKit

/// Simple class to display line views the easier way possible
///  Can select the orientation and thickness
///  To display it horizontally or vertically
public final class SimpleLineView: NonStoryboardableView {
    
    public enum Orientation {
        case vertical
        case horizontal
    }
    
    private let orientation: Orientation
    private let thickness: Int
    private let color: UIColor
    
    public init(
        orientation: Orientation = .horizontal,
        thickness: Int = 1,
        color: UIColor = HWUIKit.shared.colors.grayColor
    ) {
        self.orientation = orientation
        self.thickness  = thickness
        self.color = color
        super.init()

        backgroundColor = color
        setContentHuggingPriority(.required, for: .vertical)
    }

    public override func updateConstraints() {
        super.updateConstraints()
        applyStyles()
    }

    private func applyStyles() {
        
        switch orientation {
        case .vertical:
            snp.makeConstraints { make in
                make.width.equalTo(thickness)
            }
        case .horizontal:
            snp.makeConstraints { make in
                make.height.equalTo(thickness)
            }
        }
    }
}
