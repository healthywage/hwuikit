//
//  CheckBox.swift
//  
//
//  Created by David on 30/06/2023.
//

import UIKit
import Combine

public class CheckBox: NonStoryboardableControl {
    public override init() {
        super.init()
        backgroundColor = .white
        addSubview(imageViewPlaceholder)
        addSubview(label)
        imageViewPlaceholder.addSubview(imageView)

        label.numberOfLines = 0
        imageView.tintColor = .orange

        let labelTapGesture = UITapGestureRecognizer(
            target: self, action: #selector(labelTapped)
        )
        label.addGestureRecognizer(labelTapGesture)
        label.isUserInteractionEnabled = true

        let boxTapGesture = UITapGestureRecognizer(
            target: self, action: #selector(boxTapped)
        )
        imageViewPlaceholder.addGestureRecognizer(boxTapGesture )
    }

    public var title: String {
        get { label.text ?? "" }
        set { label.text = newValue }
    }

    public var attributedTitle: NSAttributedString? {
        didSet { label.attributedText = attributedTitle }
    }

    private let label = HWLabel(
        withStyle: .regular(withSize: 19),
        andTextColor: .black
    )

    private lazy var imageViewPlaceholder: UIView = {
        let view: UIView = .init()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = HWUIKit.shared.colors.lightGrayButtonColor
        view.layer.borderColor = UIColor.darkGray
            .withAlphaComponent(0.4).cgColor
        view.layer.borderWidth = 1
        view.layer.cornerRadius = 6
        return view
    }()

    private let imageView: ImageView = .init()

    private func updateUI() {
        if isSelected {
            let images = HWUIKit.shared.images
            imageView.image = images.checkmark
                .withRenderingMode(.alwaysTemplate) // Selected
        } else {
            imageView.image = .init() // Not.
        }
    }

    // MARK: - Publishers
    @objc private func labelTapped() {
        onLabelTapSubject.send()
    }

    private let onLabelTapSubject = PassthroughSubject<Void, Never>()
    public var onLabelTapPublisher: AnyPublisher<Void, Never> {
        onLabelTapSubject.eraseToAnyPublisher()
    }

    @objc private func boxTapped() {
        isSelected.toggle()
        onSelectionSubject.send(isSelected)
    }

    private let onSelectionSubject = PassthroughSubject<Bool, Never>()
    public var onSelection: AnyPublisher<Bool, Never> {
        onSelectionSubject.eraseToAnyPublisher()
    }

    // MARK: - Overrides

    public override var isSelected: Bool {
        didSet { updateUI() }
    }

    // MARK: - UpdateConstraints

    public override func updateConstraints() {
        super.updateConstraints()

        imageViewPlaceholder.snp.makeConstraints { make in
            make.size.equalTo(27)
            make.centerY.equalToSuperview()
            make.left.equalToSuperview()
        }
        imageView.snp.makeConstraints { make in
            make.center.equalToSuperview()
        }

        label.snp.makeConstraints { make in
            make.top.bottom.equalToSuperview()
            make.left.equalTo(imageViewPlaceholder.snp.right)
                .offset(Sizes.smallPadding)
            make.right.equalToSuperview()
        }
    }
}
