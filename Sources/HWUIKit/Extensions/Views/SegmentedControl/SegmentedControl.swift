//
//  SegmentedControl.swift
//  
//
//  Created by David on 09/08/2023.
//

import Stevia
import Combine
import UIKit

public class SegmentedControl: NonStoryboardableView {

    public let segmentedButtons: [SegmentedButton]
    private let scrollableListView = ScrollableListView(
        viewDistribution: .horizontal,
        scrollDirection: .rightToLeft,
        contentInset: 0
    )

    public init(segmentedbuttons: [SegmentedButton]) {
        self.segmentedButtons = segmentedbuttons
        if let selectedButton = segmentedbuttons.first(where: { $0.isSelected } ) {
            self.currentSelectedButton = selectedButton
        } else {
            let first = segmentedbuttons.first!
            first.isSelected = true
            currentSelectedButton = first
        }

        super.init()
        subviews(scrollableListView)
        segmentedbuttons.forEach { button in
            self.scrollableListView.addArrangedSubview(button, withCustomSpacing: 0)
            let screen = UIScreen.main.bounds.width
            button.width(screen/CGFloat(segmentedbuttons.count))
            button.selectionPublisher
                .sink {  [weak self] _ in self?.selectionDidChange(for: button) }
                .store(in: &self.cancellables)
        }
    }

    private var cancellables: Set<AnyCancellable> = .init()

    public var selectedItemPublisher: AnyPublisher<Int, Never> {
        selectedItemSubject.eraseToAnyPublisher()
    }
    private let selectedItemSubject = PassthroughSubject<Int, Never>()

    private var currentSelectedButton: SegmentedButton
    private func selectionDidChange(for button: SegmentedButton) {
        guard
            button.isSelected,
            button != currentSelectedButton
        else { return }
        currentSelectedButton.isSelected = false
        self.currentSelectedButton = button
        selectedItemSubject.send(button.id)
    }

    // MARK: - UpdateConstraints

    public override func updateConstraints() {
        super.updateConstraints()

        layout {
            1
            |-0-scrollableListView-0-|
            1
        }
    }
}

