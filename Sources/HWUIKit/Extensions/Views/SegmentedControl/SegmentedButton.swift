//
//  File.swift
//  
//
//  Created by David on 09/08/2023.
//

import UIKit
import Stevia
import Combine

public class SegmentedButton: NonStoryboardableControl {

    public let id: Int
    public var image: UIImage {
        didSet { imageView.image = image}
    }
    public var selectedImage: UIImage?

    public var title: String {
        didSet {
            titleLabel.text = title
            selectedTitleLabel.text = title
        }
    }

    public init(id: Int, title: String, image: UIImage, selectedImage: UIImage?) {
        self.image = image
        self.selectedImage = selectedImage
        self.title = title
        self.id = id

        super.init()
        imageView.image = image
        titleLabel.text = title
        selectedTitleLabel.text = title
        subviews(imageView,titleLabel, selectedTitleLabel, lineView, buttonProxy)
        backgroundColor = .white
        updateUI()

        titleLabel.textAlignment = .center
        selectedTitleLabel.textAlignment = .center
        imageView.contentMode = .center
        buttonProxy.translatesAutoresizingMaskIntoConstraints = false
        buttonProxy.addTarget(self, action: #selector(tapped), for: .touchUpInside)
    }

    private let buttonProxy: UIButton = .init()

    public var selectionPublisher: AnyPublisher<Bool, Never> {
        selectionSubject.eraseToAnyPublisher()
    }
    private let selectionSubject = PassthroughSubject<Bool, Never>()

    private let imageView: ImageView = .init()
    private lazy var titleLabel: HWLabel = {
        let colors = HWUIKit.shared.colors
        return HWLabel(withStyle: .bold(withSize: 14), andTextColor: colors.blueButtonColor)
    }()

    private lazy var selectedTitleLabel: HWLabel = {
        let colors = HWUIKit.shared.colors
        return HWLabel(withStyle: .bold(withSize: 14), andTextColor: colors.segmentedOrange)
    }()

    private lazy var lineView = SimpleLineView(
        orientation: .horizontal,
        thickness: 2,
        color: HWUIKit.shared.colors.segmentedOrange
    )

    @objc private func tapped() {
        isSelected = true
    }

    // MARK: - Overrides

    public override var isSelected: Bool {
        didSet {
            updateUI()
            if oldValue != isSelected {
                selectionSubject.send(isSelected)
            }
        }
    }

    // MARK: - Func

    private func updateUI() {
        UIView.animate(withDuration: 0.35) {
            self.lineView.alpha = self.isSelected ? 1 : 0.01
            self.selectedTitleLabel.alpha = self.isSelected ? 1 : 0.01
            self.titleLabel.alpha = !self.isSelected ? 1 : 0.01
        }

        if let selectedImage {

            let toImage = self.isSelected ? selectedImage : self.image
            UIView.transition(
                with: imageView,
                duration: 0.35,
                options: .transitionCrossDissolve,
                animations: { self.imageView.image = toImage },
                completion: nil
            )
        } else {
            imageView.image = image
        }

    }

    // MARK: - UpdateConstraints

    public override func updateConstraints() {
        super.updateConstraints()
        layout(
            Sizes.extraSmallPadding,
            |-imageView-Sizes.extraSmallPadding-titleLabel-|,
            Sizes.extraSmallPadding,
            |-0-lineView-0-|,
            0
        )
        selectedTitleLabel.followEdges(titleLabel)
        buttonProxy.followEdges(self)
    }
}
