//
//  UIDevice+Extensions.swift
//  HWUIKit
//
//  Created by Kimi on 21/10/21.
//

import UIKit
import Foundation

public extension UIDevice {
    
    var hasNotch: Bool {
        var bottom : CGFloat = 0.0
        return bottom > 0.0
    }
    
    var statusBarHeight: CGFloat {
        return UIApplication.shared.statusBarFrame.size.height
    }
    
    var topSpaceToScreen: CGFloat {
        if !UIDevice.current.hasNotch {
            return 0.0
        }
        
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            return 24.0
        case .phone:
            switch (UIDevice.current.orientation) {
            case .landscapeLeft, .landscapeRight:
                return 32.0
            case .portrait, .portraitUpsideDown, .unknown:
                return 44.0
            default:
                return 44.0
            }
        default:
            break
        }
        return 0.0
    }
    
    var bottomSpaceToScreen: CGFloat {
        if !UIDevice.current.hasNotch {
            return 0.0
        }
        
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            return 20.0
        case .phone:
            switch (UIDevice.current.orientation) {
            case .landscapeLeft, .landscapeRight:
                return 0.0
            case .portrait, .portraitUpsideDown, .unknown:
                return 34.0
            default:
                return 34.0
            }
        default:
            break
        }
        return 0.0
    }
    
}
