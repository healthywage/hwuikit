//
//  SegmentedButtonTests.swift
//  
//
//  Created with love by David on 09/08/2023.
//

@testable import HWUIKit
import SnapshotTesting
import XCTest

final class SegmentedButtonTests: XCTestCase {

    override func setUpWithError() throws {
        isRecording = true
    }

    func testBasics() throws {
        let segmentedButton = SegmentedButton(id: 0, title: "[[TEST]]", image: .add)
        segmentedButton.setNeedsUpdateConstraints()
        segmentedButton.layoutSubviews()

        assertSnapshot(matching: segmentedButton, as: .image)
        segmentedButton.isSelected = true

        assertSnapshot(matching: segmentedButton, as: .image)
    }

    func testSegmentedControl() throws {
        let button1 = SegmentedButton(id:1, title: "[[TEST1]", image: .add)
        let button2 = SegmentedButton(id:2, title: "[[TEST2]", image: .remove)
        let button3 = SegmentedButton(id:3, title: "[[TEST3]", image: .checkmark)
        let button4 = SegmentedButton(id:4, title: "[[TEST4444444]", image: .strokedCheckmark)

        let segmentedControl = SegmentedControl(segmentedbuttons: [button1, button2, button3, button4])
        segmentedControl.setNeedsUpdateConstraints()

        assertSnapshot(matching: segmentedControl, as: .image)

    }
}
