import XCTest
@testable import HWUIKit

final class HWUIKitTests: XCTestCase {
    func testExample() throws {
        let readmeURL = HWUIKit.shared.resourceBundle.url(forResource: "README", withExtension: "md")        
        XCTAssertNotNil(readmeURL)
    }
    
    func testCanLoadColors() throws {
        let color = HWUIKit.shared.colors.grayColor
        XCTAssertNotNil(color)
    }
}
