//
//  CheckBoxTests.swift
//  
//
//  Created by David Muñoz on 30/06/2023.
//

@testable import HWUIKit
import SnapshotTesting
import XCTest

final class CheckBoxTests: XCTestCase {

    override func setUpWithError() throws {
        isRecording = false
    }

    func testBasics() throws {
        let checkBox = CheckBox()
        checkBox.title = "[test title]"
        assertSnapshot(matching: checkBox, as: .image)
        checkBox.isSelected = true
        assertSnapshot(matching: checkBox, as: .image)

        let attributedTitle = NSMutableAttributedString(string: "[[Test]]")

        let attributedText = NSMutableAttributedString(
            string: "[more test]",
            attributes: [
                .foregroundColor: UIColor.purple
            ]
        )
        attributedTitle.append(attributedText)
        checkBox.attributedTitle = attributedTitle
        assertSnapshot(matching: checkBox, as: .image)
    }
}
